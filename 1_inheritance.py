# even tho inheritance is shit in every programming language in existance
# lets get this over with
class Animal(object):
    def __init__(self, kind: str, voice: str) -> None:
        self.kind = kind
        self.voice = voice
        self.i_will_be_overridden = "not just yet..."

    def speak(self):  # mute animals shall not pass!
        print("Animal of kind:", self.kind, "Says:", self.voice)


class Dog(Animal):
    def __init__(self, kind: str, voice: str) -> None:
        # so each class object has some sort of objects hashmap,
        # and this function call initializes self (this objects hashmap),
        # with parents hashmap, using parent constructor,
        # therefore it clones every method object from parent into self hashmap
        super().__init__(kind, voice)

    def speak(self):
        print("This function is overriden by super().__init__(kind, voice)")


def main():
    dog = Dog("dog, idk", "woof woof mfs")
    dog.speak()
    print(dog.i_will_be_overridden)
    dog.i_will_be_overridden = "NOW"
    print(dog.i_will_be_overridden)


if __name__ == "__main__":
    main()
