# same as class Dog(object):
class Dog:
    # voice: str # no need to predefine class field
    def __init__(self, voice: str) -> None:
        self.voice = voice  # i guess every dog speaks a little different

    # no "self" required, can be called as global object
    def speak():
        print("woof woof")

    def self_speak(self):
        print(self.voice)

    def add_name(self, name: str):
        # attributes are added at runtime, cool!
        self.name = name

    # dont even ask...
    def square(self, num: int) -> int:
        return num * num


def main():
    print(
        "Base classes of Dog:", Dog.__bases__
    )  # still prints <class 'object'> even tho no parent specified

    # dog = Dog("woof woof mfs")
    # is short for
    #
    # dog = Dog.__new__(Dog)
    # dog.__init__("bruh")

    Dog.speak()

    dog = Dog("woof woof mfs")
    dog.self_speak()

    # functions are also objects
    print(dog.square(3))
    dog.square = lambda self, num: num**3
    print(dog.square(dog, 3))  # xd

    # print(dog.name) # AttributeError: 'Dog' object has no attribute 'name'
    dog.add_name("willy")
    print(dog.name)  # ok


if __name__ == "__main__":
    main()
