from typing import Any
from copy import copy, deepcopy


def addr(x: Any) -> str:
    return hex(id(x))


def mutability_demo():
    # in python lists are mutable objects
    # and strings/integers are NOT
    l: list[int] = [1, 2, 3]
    s: str = "Hello, world!"
    i: int = 5

    print("Before change:", type(l), addr(l))
    l.append(4)
    print("After change:", type(l), addr(l))  # same

    print("Before change:", type(s), addr(s))
    s.capitalize()
    print("After change:", type(s), addr(s))  # same, even tho is immutable

    print("Before change:", type(i), addr(i))
    i += 5
    print("After change:", type(i), addr(i))  # different


def is_operator():
    # "is" operator compares memory locations stored within python objects
    l1: list[int] = [1, 2, 3]
    l2 = l1
    l2.append(5)

    # even tho l1 and l2 probably have different stack frames,
    # they still point to the same (heap?) list data.
    # therefore:
    print(l1 is l2)  # True

    # create a slice of the whole list (shallow copy it)
    l3 = l1[:]  # or copy(l1), which is the same thing in this case
    print(l3 is l1)


def shallow_vs_deep_copy():
    l1: list[list[int]] = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    l2 = l1

    # same thing, just copy stackframe, dont copy (heap?) list data
    print(l2 is l1)  # True

    l3 = l1[:]  # same as copy(l1)
    # False, since we copied underlying list of list objects,
    # but object still point to another list of ints, therefore...
    print(l1 is l3)  # False

    # l3 and l1 point to different locations, true...
    # BUT underlying objects (refs) still point to shared data (list[int])
    l3[0][0] = 555
    print(l1[0][0])  # 555

    # to create BRAND NEW list, lets "deep copy" l1,
    # by copying underlying objects and their data...

    l4 = deepcopy(l1)
    print(l1 is l4)  # False, since we copied the list contents...

    l4[1][0] = 111
    print(l4[1][0], l1[1][0])  # 111 4, since we copied list of list objects,
    # AND the objects itself (their data)

    # it all comes down to good old pointer knowledge i guess...


def copying():
    mutability_demo()
    is_operator()
    shallow_vs_deep_copy()


if __name__ == "__main__":
    copying()
