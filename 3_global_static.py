from typing import Any


class Animal:
    animals = []  # global between all instances of this class (and its children)
    num = 5  # Animal.num will return 5.
    num_glob = 5

    # before __init__ something like this happens:
    # self.animals = Animal.animals # mutable
    # self.num = Animal.num # immutable
    # self.num_glob = Animal.num_glob # immutable
    def __init__(self, name: str) -> None:
        self.name = name
        Animal.animals.append(name)  # same thing
        self.animals.append(name)  # self.animals is Animal.animals
        # self.num is not Animal.num
        self.num += 1  # cat.num will return 6!
        Animal.num_glob += 1

        print(self.animals is Animal.animals)  # True
        print(self.num is Animal.num)  # False

    def get_self(self) -> Any:
        return self

    @classmethod
    def print_animals(cls):
        # cls is <class '__main__.Animal'>
        print(cls.animals)

    @staticmethod
    def do_flip():
        print("*does flip*")


def main():
    cat = Animal("cat")
    dog = Animal("dog")

    # remember - different stack frame, same (heap?) data
    # for more see copying.py
    print("self check:", cat == cat.get_self())  # True

    print(cat.animals)
    print(dog.animals)
    print(Animal.animals)
    Animal.print_animals()  # passes <class 'Animal'> in cls
    Animal.do_flip()  # passes nothing, class is simply used as a namespace here

    print(cat.num)  # 6
    print(dog.num)  # 6
    print(Animal.num)  # 5 ??? wtf
    # why is list global across all objects,
    # and int is not?
    # this is because of an objects immutability property
    # list in this case is mutable
    # and int is immutable
    # for more see copying.py

    print(Animal.num_glob)  # 7


if __name__ == "__main__":
    main()
