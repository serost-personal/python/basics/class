class Point:
    def __init__(self, x: float, y: float) -> None:
        self.x = x
        self.y = y

    def __add__(self, other):
        add_point = lambda self, other: Point(self.x + other.x, self.y + other.y)
        add_single = lambda self, other: Point(self.x + other, self.y + other)

        add_funcs = {
            Point: add_point,
            int: add_single,
            float: add_single,
        }

        add_func = add_funcs.get(type(other))
        if add_func is None:
            return None
        return add_func(self, other)

    def __str__(self) -> str:
        return "Point({0},{1})".format(self.x, self.y)


def main():
    p1 = Point(1, 2)
    p2 = Point(3, 4)
    p3 = p1 + p2
    print(p3)
    p4 = p1 + 5
    print(p4)

    Point.__mul__ = lambda self, other: Point(self.x * other.x, self.y * other.y)

    p5 = p3 * p4
    print(p5)


if __name__ == "__main__":
    main()
