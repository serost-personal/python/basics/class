from dataclasses import dataclass, field

from inspect import getmembers

# global vairables in this case specify what to generate
# dataclass does some metaprogramming under the hood,
# if they have default values, they can be accessed via ClassName.attr
# otherwise dataclass unsets those attributes with delattr()

# code snippet from dataclasses.py, to back it up (comments snipped):
#
# for f in cls_fields:
#     fields[f.name] = f
#     if isinstance(getattr(cls, f.name, None), Field):
#         if f.default is MISSING:
#             delattr(cls, f.name)
#         else:
#             setattr(cls, f.name, f.default)
#
# for more details, check the file yourself!


# frozen=True makes it hashable and immutable
# unsafe_hash=True makes it hashable and mutable
# order=True makes it orderable (e.g __le__, __ge__)
@dataclass(frozen=True, order=False)
class Person:
    dont_init_me: str = field(init=False)
    age: int = field(repr=False)  # dont include in __repr__
    first_name: str = "John"  # default should come after non default
    last_name: str = field(default="Doe")  # same just setting plain default

    # since dataclass generates __init__ function, we dont want to overwrite
    # it, but here is solution that they provide: __post_init__, which runs
    # after init is made
    def __post_init__(self):
        self.dont_init_me = "NOW"
        print("now dont_init_me can be used!")


def main():
    p1 = Person(25, "Willy", "Diamond")

    for p in getmembers(p1):
        print(p)

    print(p1)  # no age, since
    print(Person.first_name)  # John
    print(p1.dont_init_me)
    # print(Person.age) # AttributeError: type object 'Person' has no attribute 'age'


if __name__ == "__main__":
    main()
